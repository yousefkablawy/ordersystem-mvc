-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 07, 2022 at 07:02 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mvcframework`
--

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `orderid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `productquantity` int(11) NOT NULL,
  `orderdate` timestamp NOT NULL DEFAULT current_timestamp(),
  `productid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`orderid`, `userid`, `productquantity`, `orderdate`, `productid`) VALUES
(20, 11, 2, '2022-01-07 17:36:38', 9),
(21, 11, 1, '2022-01-07 17:36:48', 9),
(22, 11, 1, '2022-01-07 17:37:20', 11),
(23, 11, 1, '2022-01-07 17:38:06', 9),
(24, 12, 1, '2022-01-07 17:43:19', 9),
(25, 12, 1, '2022-01-07 17:43:24', 11);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `productid` int(11) NOT NULL,
  `productname` varchar(255) NOT NULL,
  `productprice` float NOT NULL,
  `productdesc` text NOT NULL,
  `filename` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`productid`, `productname`, `productprice`, `productdesc`, `filename`) VALUES
(9, 'product', 222.6, 'sdadsadass sfasd AD afd esadas ASZX', 'phone5.jpg'),
(11, 'product', 10, 'this is a mobile phone', 'phone2.jpg'),
(12, 'product', 15.6, 'this is a prsaf asdfwas', 'phone3.png'),
(13, 'product', 20.5, 'this is a new product', 'phone6.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userid` int(11) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `useremail` varchar(255) NOT NULL,
  `userpassword` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userid`, `fullname`, `username`, `useremail`, `userpassword`, `created_at`) VALUES
(10, 'Ahmad kb', 'ahmad', 'ykablawy@gmail.com', '$2y$10$t/VlAQJ.BBsWwlQy2XI/1.M13nMvem2RttrlBAcKfCpRlU8IYgg9m', '2022-01-02 15:38:58'),
(11, 'nawar mokaid', 'nawar', 'nawar@gmail.com', '$2y$10$lzfBDcK2p8y08Lu1OrP5Z.lzKL8vJL7LbKp23z/q6B1zJ60i6ITDq', '2022-01-02 19:47:32'),
(12, 'yousef', 'yousef', 'hani@gmail.com', '$2y$10$9VXu.LlM6066Me.ZXXIAg.E1/0ILlssqmfYuI2lKQ4Mp9N2XfZA/y', '2022-01-03 20:34:30'),
(13, 'yousef', 'Admin', 'aaa@gmail.com', '$2y$10$AkMuKRPxF.paXTgwUjhCYOoqDA0G3pwxuxc4pBpuVxof0pOSS7eHq', '2022-01-04 15:20:04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`orderid`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`productid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `orderid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `productid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
