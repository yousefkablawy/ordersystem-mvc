<?php 
class Product
{
    private $db;

    public function __construct()
    {
        $this->db=new Database();
    }

    public function getProducts()
    {
        $this->db->query("SELECT * FROM products");
        $result=$this->db->resultSet();
        return $result;
    }
    public function getSingleProduct($productId)
    {
        $this->db->query('SELECT * FROM products WHERE products.productid =:productId');
        $this->db->bind(':productId',$productId);
        $result=$this->db->singleRow();
        return $result;
    }
    public function addProduct($data)
    {
        $this->db->query("INSERT INTO products(productname,productprice,productdesc,filename) VALUES (:productname,:productprice,:productdesc,:filename)");
        $this->db->bind(':productname',$data['productname']);
        $this->db->bind(':productprice',$data['productprice']);
        $this->db->bind(':productdesc',$data['productdesc']);
        $this->db->bind(':filename',$data['fileName']);
        if($this->db->execute())
         {
             return true;
         }
         else
         {
             return false;
         }
    }
    public function deleteProduct($productId)
    {
        $this->db->query('DELETE  FROM products WHERE products.productid =:productId');
        $this->db->bind(':productId',$productId);
        if($this->db->execute())
         {
             return true;
         }
         else
         {
             return false;
         }
    }

   
    public function updateProduct($data)
    {
        $this->db->query('UPDATE products SET productname =:productname, productprice=:productprice ,productdesc=:productdesc,filename=:filename WHERE productid = :productid');

        $this->db->bind(':productid',$data['productid']);
        $this->db->bind(':productname', $data['productname']);
        $this->db->bind(':productprice', $data['productprice']);
        $this->db->bind(':productdesc', $data['productdesc']);
        $this->db->bind(':filename', $data['fileName']);

        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }


    
    
}