
<?php 
class Order
{
    private $db;

    public function __construct()
    {
        $this->db=new Database();
    }

    public function getorders()
    {
        $this->db->query('SELECT orders.orderdate,orders.productquantity,products.productname,products.productprice,products.productdesc,products.filename 
        FROM orders 
        JOIN products 
            ON orders.productid=products.productid 
        WHERE userid=:userid');
        $this->db->bind(':userid',$_SESSION['user_id']);
        $result=$this->db->resultSet();
        return $result;
    }

    public function createorder($data)
    {
        $this->db->query('INSERT INTO orders(userid,productid,productquantity) VALUES(:userid,:productid,:productquantity)');
        $this->db->bind(':productid',$data["productid"]);
        $this->db->bind(':userid',$data["userid"]);
        $this->db->bind(':productquantity', $data['productquantity']);
        if($this->db->execute())
         {
             return true;
         }
         else
         {
             return false;
         }
    }
}