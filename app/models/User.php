<?php 
 class User
 {
     private $db;
     public function __construct()
     {
         $this->db=new Database;
     }

     public function getUsers()
     {
         $this->db->query("SELECT * FROM users");
         $result=$this->db->resultSet();

         return $result;
     }

     public function findUserByEmail($email)
     {
         $this->db->query('SELECT * FROM users WHERE useremail=:email');
         $this->db->bind(':email',$email);
         if($this->db->singleRow())
         {
            return true;
         }
         else
         {
            return false;
         }
     }
     
     public function register($data)
     {
         $this->db->query('INSERT INTO users(fullname,username,useremail,userpassword) VALUES(:fullname,:username,:email,:password)');
         $this->db->bind(':fullname',$data['fullname']); 
         $this->db->bind(':username',$data['username']); 
         $this->db->bind(':email',$data['email']); 
         $this->db->bind(':password',$data['password']); 
         
         if($this->db->execute())
         {
             return true;
         }
         else
         {
             return false;
         }
     }

     public function login($username,$password)
     {
         $this->db->query('SELECT * FROM users WHERE username=:username OR useremail=:username');
         $this->db->bind(':username',$username);
         if($row=$this->db->singleRow())
         {
            $hashedPassword=$row->userpassword;
            if(password_verify($password,$hashedPassword))
            {
                return $row;
            }
            else
            {
                return false;
            }
         }
         
     }
 }