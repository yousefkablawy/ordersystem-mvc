<?php 
 class Orders extends Controller
 {
    public function __construct()
    {
        $this->orderModel=$this->model('Order');
    }
    public function createorder()
    {
        $data=[
            'productid'=>'',
            'userid'=>$_SESSION['user_id'],
            'productquantity'=>'',
            'productquantityError'=>'',

        ];
        if($_SERVER['REQUEST_METHOD']=='POST')
            {
                $_POST=filter_input_array(INPUT_POST,FILTER_SANITIZE_STRING);

                $data=[
                    'productid'=>trim($_POST['productId']),
                    'productquantity'=>trim($_POST['quantity']),
                    'userid'=>$_SESSION['user_id'],
                    'productquantityError'=>'',
                ];
                // validate the quantity
                if(empty($data['productquantity']))
                {
                    $data['productquantity']=1;
                }
                else
                {
                        if(!preg_match("/([0-9]+)(\.[0-9]{1,2})?/",$data['productquantity']))
                            {
                                $data['productquantityError']="please Enter a Number!!";
                            }
                }
                $orderCreated=$this->orderModel->createorder($data);
                if($orderCreated)
                {
                    header('location:'.URLROOT.'/orders/myorder');
                }
                else
                {
                    
                    die('something went wrong');
                }
            }
    }

    public function myorder()
    {
        $orders=$this->orderModel->getorders();
        $data=['orders'=>$orders];
        $this->view('orders/myorder',$data);
    }
 }