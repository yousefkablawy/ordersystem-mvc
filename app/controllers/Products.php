<?php 
    class Products extends Controller 
    {
        public function __construct()
        {
            $this->productModel=$this->model('Product');
        }

        public function index()
        {
            $products=$this->productModel->getProducts();
            $data=[
                'products'=>$products
            ];
            $this->view('products/index',$data);
        }

        public function addProduct()
        {
            $data=[
                'productname'=>'',
                'productprice'=>'',
                'productdesc'=>'',
                'fileName'=>'',
                'productnameError'=>'',
                'productpriceError'=>'',
                'productdescError'=>'',
                'productimageError'=>'',

            ];
            if($_SERVER['REQUEST_METHOD']=='POST')
            {
                $_POST=filter_input_array(INPUT_POST,FILTER_SANITIZE_STRING);
                $data=[
                'productname'=>trim($_POST['productname']),
                'productprice'=>trim($_POST['productprice']),
                'productdesc'=>trim($_POST['productdesc']),
                'fileName'=>$_FILES["productimage"]["name"],
                'productnameError'=>'',
                'productpriceError'=>'',
                'productdescError'=>'',
                'productimageError'=>'',

                ];
                $fileTempName = $_FILES["productimage"]["tmp_name"];    
                $fileSize= $_FILES["productimage"]["size"];
                $fileError=$_FILES["productimage"]["error"];
                $fileExt=explode('.',$data['fileName']);
                $fileActualExt=strtolower(end($fileExt));
                $allowedExt=array('jpg','jpeg','png');
                
                // IMAGE VALIDATE
                    if(!empty($data['fileName']))
                        {
                            if(in_array($fileActualExt,$allowedExt))
                                {
                                    if($fileError===0)
                                    {
                                        if($fileSize< 5000000)
                                        {
                                            $fileDestination= dirname(APPROOT)."/public/img/".$data['fileName'];
                                            $uploadImage=move_uploaded_file($fileTempName,$fileDestination);
                                            if(!$uploadImage)
                                            {
                                                $data['productimageError']="sorry this is an error while uploading image";
                                            }
                                        }
                                        else
                                        {
                                        $data['productimageError']="this image is too big";
                                        }
                                    }
                                    else
                                    {
                                        $data['productimageError']="there was a problem uploading this file";
                                    }

                                }   
                            else
                                {
                                    $data['productimageError']="the extension of an image must be jpg,jpeg,png";
                                }
                         }
                    else
                        {
                            $data['productimageError']="you must insert an image";
                        }
                                
                //productname           
                if(empty($data['productname']))
                {
                    $data['productnameError']="please Enter a productname";
                }
                else
                {
                        if(!preg_match("/^([a-zA-Z0-9])*$/",$data['productname']))
                            {
                                $data['productnameError']="please Enter a Valid productname";
                            }
                }
                //productprice            
                if(empty($data['productprice']))
                {
                    $data['productpriceError']="please Enter a productprice";
                }
                else
                {
                        if(!preg_match("/([0-9]+)(\.[0-9]{1,2})?/",$data['productprice']))
                            {
                                $data['productpriceError']="please Enter a Valid productprice";
                            }
                }
                //productdesc
                if(empty($data['productdesc']))
                {
                        $data['productdescError']="please Enter a productdesc";
                }
                else
                {
                                if(!preg_match("/^([a-zA-Z ])*$/",$data['productdesc']))
                                {
                                    $data['productdescError']="please Enter a Valid productdescription";
                                }
                }
                // check if there is no error
                if(empty($data['productnameError']) && empty($data['productpriceError']) && empty($data['productdescError']) && empty($data['productimageError']))
                {
                    $productAdded=$this->productModel->addProduct($data);
                    if($productAdded)
                    {
                        header('location:'.URLROOT.'/products/index');
                    }
                    else
                    {
                        die('something went wrong try again please');
                    }
                }
                
            }
            $this->view('products/addProduct',$data);
        }
        

        public function deleteProduct($productId)
        {
            $isDeleted=$this->productModel->deleteProduct($productId);
            if($isDeleted)
            {
                header('location:'.URLROOT.'/products/index');
            }
            else
            {
                die('something went wrong try again please');
            }
        }
        public function updateProduct($productId)
        {
            $product=$this->productModel->getSingleProduct($productId);
                $data=[
                    'productname'=>$product->productname,
                    'productprice'=>$product->productprice,
                    'productdesc'=>$product->productdesc,
                    'fileName'=>$product->filename,
                    'productid'=>$productId,
                    'productnameError'=>'',
                    'productpriceError'=>'',
                    'productdescError'=>'',
                    'productimageError'=>'',

            ];
            $filename=$data['fileName'];
            if($_SERVER['REQUEST_METHOD']=='POST')
            {
                $_POST=filter_input_array(INPUT_POST,FILTER_SANITIZE_STRING);
                $data=[
                'productname'=>trim($_POST['productname']),
                'productprice'=>trim($_POST['productprice']),
                'productdesc'=>trim($_POST['productdesc']),
                'fileName'=>$_FILES["productimage"]["name"],
                'productid'=>$productId,
                'productnameError'=>'',
                'productpriceError'=>'',
                'productdescError'=>'',
                'productimageError'=>'',

                ];
                $fileTempName = $_FILES["productimage"]["tmp_name"];    
                $fileSize= $_FILES["productimage"]["size"];
                $fileError=$_FILES["productimage"]["error"];
                $fileExt=explode('.',$data['fileName']);
                $fileActualExt=strtolower(end($fileExt));
                $allowedExt=array('jpg','jpeg','png');
                
                // IMAGE VALIDATE
                    if(!empty($data['fileName']))
                        {
                            if(in_array($fileActualExt,$allowedExt))
                                {
                                    if($fileError===0)
                                    {
                                        if($fileSize< 5000000)
                                        {
                                            $fileDestination= dirname(APPROOT)."/public/img/".$data['fileName'];
                                            $uploadImage=move_uploaded_file($fileTempName,$fileDestination);
                                            if(!$uploadImage)
                                            {
                                                $data['productimageError']="sorry this is an error while uploading image";
                                            }
                                        }
                                        else
                                        {
                                        $data['productimageError']="this image is too big";
                                        }
                                    }
                                    else
                                    {
                                        $data['productimageError']="there was a problem uploading this file";
                                    }

                                }   
                            else
                                {
                                    $data['productimageError']="the extension of an image must be jpg,jpeg,png";
                                }
                         }
                    else
                        {
                            $data['productimageError']="";
                            $data['fileName']=$filename;
                        }
                                
                //productname           
                if(empty($data['productname']))
                {
                    $data['productnameError']="please Enter a productname";
                }
                else
                {
                        if(!preg_match("/^([a-zA-Z0-9])*$/",$data['productname']))
                            {
                                $data['productnameError']="please Enter a Valid productname";
                            }
                }
                //productprice            
                if(empty($data['productprice']))
                {
                    $data['productpriceError']="please Enter a productprice";
                }
                else
                {
                        if(!preg_match("/([0-9]+)(\.[0-9]{1,2})?/",$data['productprice']))
                            {
                                $data['productpriceError']="please Enter a Valid productprice";
                            }
                }
                //productdesc
                if(empty($data['productdesc']))
                {
                        $data['productdescError']="please Enter a productdesc";
                }
                else
                {
                                if(!preg_match("/^([a-zA-Z ])*$/",$data['productdesc']))
                                {
                                    $data['productdescError']="please Enter a Valid productdescription";
                                }
                }
                // check if there is no error
                if(empty($data['productnameError']) && empty($data['productpriceError']) && empty($data['productdescError']) && empty($data['productimageError']))
                {
                   $updatedProduct=$this->productModel->updateProduct($data);
                   if($updatedProduct)
                        {
                            header('location:'.URLROOT.'/products/index');
                        }
                        else
                        {
                            die('something went wrong try again please');
                        }

                }
                
            }
                
            $this->view('products/updateProduct',$data);
            

           

        }

}