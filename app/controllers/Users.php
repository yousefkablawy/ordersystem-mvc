<?php 
class Users extends Controller
{
    public function __construct()
    {
        $this->userModel=$this->model('User');
    }
    
    public function login ()
    {
        $data=[
            'username'=>'',
            'password'=>'',
            'usernameError'=>'',
            'passwordError'=>'',
        ];
        if($_SERVER['REQUEST_METHOD']=='POST')
        {
            $_POST=filter_input_array(INPUT_POST,FILTER_SANITIZE_STRING);
            $data=[
                'username'=>trim($_POST['username']),
                'password'=>trim($_POST['password']),
                'usernameError'=>'',
                'passwordError'=>'',
            ];
            // validate username
            if(empty($data['username']))
            {
                $data['usernameError']='Please Enter Your UserName';
            }
            // validate password
            if(empty($data['password']))
            {
                $data['passwordError']='Please Enter A Password';
            }
            // check if there is an error
            if( empty($data['usernameError']) && empty($data['passwordError']))
            {
                $loggedInUser=$this->userModel->login($data['username'],$data['password']);
                if($loggedInUser)
                {
                    $this->createUserSession($loggedInUser);
                }
                else
                {
                    $data['passwordError']='password or username are incorrect';
                }
            }
        }
        $this->view('users/login',$data);
    }

    public function signup()
    {
        $data=[
            'fullname'=>'',
            'username'=>'',
            'email'=>'',
            'password'=>'',
            'confirmPassword'=>'',
            'fullnameError'=>'',
            'usernameError'=>'',
            'emailError'=>'',
            'passwordError'=>'',
            'confirmPasswordError'=>'',
        ];
        
        if($_SERVER['REQUEST_METHOD']=='POST')
        {
            $_POST=filter_input_array(INPUT_POST,FILTER_SANITIZE_STRING);
            $data=[
                'fullname'=>trim($_POST['fullname']),
                'username'=>trim($_POST['username']),
                'email'=>trim($_POST['email']),
                'password'=>trim($_POST['password']),
                'confirmPassword'=>trim($_POST['confirmPassword']),
                'fullnameError'=>'',
                'usernameError'=>'',
                'emailError'=>'',
                'passwordError'=>'',
                'confirmPasswordError'=>'',
            ];
            // validate fullname
            if(empty($data['fullname']))
            {
                $data['fullnameError']='Please Enter Your FullName';
            }
            elseif(!preg_match("/^([a-zA-Z' ]+)$/",$data['fullname']))
            {
                $data['fullnameError']="please Enter Letters only";
            }
            // validate username
            if(empty($data['username']))
            {
                $data['usernameError']='Please Enter A UserName';
            }
            elseif(!preg_match("/^([a-zA-Z0-9'])*$/",$data['username']))
            {
                $data['usernameError']="the username can only be letters and numbers";
            }
            // validate email
            if(empty($data['email']))
            {
                $data['emailError']='Please Enter Your Email';
            }
            elseif(!filter_var($data['email'],FILTER_VALIDATE_EMAIL))
            {
                $data['emailError']="please Enter a Valid Email";
            }
            else
            {
                if($this->userModel->findUserByEmail($data['email']))
                {
                    $data['emailError']="this Email is already exists";
                }
            }

            // validate password
            if(empty($data['password']))
            {
                $data['passwordError']='Please Enter A Password';
            }
            else
            {
                $uppercase = preg_match('@[A-Z]@', $data['password']);
                $lowercase = preg_match('@[a-z]@', $data['password']);
                $number    = preg_match('@[0-9]@', $data['password']);
                $specialChars = preg_match('@[^\w]@', $data['password']);

                if(!$uppercase || !$lowercase || !$number || !$specialChars || strlen($data['password']) < 8)
                {
                    $data['passwordError'] ='Password should be at least 8 characters in length and should include at least one upper case letter, one number, and one special character.';
                }
            }

            // validate confirmpassword
            if(empty($data['confirmPassword']))
            {
                $data['confirmPasswordError']='Please Enter Confirm the Password';
            }
            elseif($data['password']!==$data['confirmPassword'])
            {
                $data['confirmPasswordError']="the passwords dont match";
            }

            // check if there is no Error
            if(empty($data['fullnameError']) && empty($data['usernameError']) && empty($data['emailError']) && empty($data['passwordError']) && empty($data['confirmPasswordError']))
            {
                // hashing the password
                $data['password']=password_hash($data['password'],PASSWORD_DEFAULT);
                // registering the user
                if($this->userModel->register($data))
                {
                    header('location:'.URLROOT.'/users/login');
                }
                else
                {
                    die('something went wrong');
                }
            }

        }
        $this->view('users/signup',$data);
    }

    public function createUserSession($user)
    {
        $_SESSION['user_id']=$user->userid;
        $_SESSION['username']=$user->username;
        header('location:'.URLROOT.'/pages/index');
    }

    public function logout()
    {
        unset($_SESSION['user_id']);
        unset($_SESSION['username']);
        header('location:'.URLROOT.'/users/login');
    }
}