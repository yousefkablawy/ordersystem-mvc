<?php require_once APPROOT."/views/templates/header.php";?>
<?php 
    if(!$_SESSION['username'])
    {
        header('Location:'.URLROOT.'/users/login');
    }
   
?>
<?php if($_SESSION['username']==='Admin'):?>
      <a href="<?php echo URLROOT.'/products/addProduct'?>" class='btn btn-primary mx-2'>add product</a>
<?php endif;?>
<div class="row">

  <?php foreach($data['products'] as $product): ?>
  <div class="col-lg-4">
    <div class="card mb-3" style="width: 18rem;">
      <img src="<?php echo "../public/img/".htmlspecialchars($product->filename)?>" class="card-img-top" alt="...">
      <div class="card-body">
        <h5 class="card-title"><?php echo  htmlspecialchars($product->productname)?></h5>
        <p class="card-text"><?php echo htmlspecialchars($product->productprice)?></p>
        <p class="card-text"><?php echo htmlspecialchars($product->productdesc)?></p>
        <?php if($_SESSION['username']==='Admin'):?>

                     <a href="<?php echo URLROOT . "/products/deleteProduct/" . htmlspecialchars($product->productid) ?>" class="btn btn-danger"> Delete</a>
                    <a href="<?php echo URLROOT . "/products/updateProduct/" . htmlspecialchars($product->productid) ?>" class="btn btn-success"> Edit</a>
        <?php else :?>  
                  <form action="<?php echo URLROOT.'/orders/createorder'?>" method="post">
                      <input type="hidden" name='productId' value="<?php echo  htmlspecialchars($product->productid)?>">
                      <input type="text" name="quantity">
                      <button type='submit' name='send_order' class='btn btn-danger'>Order</button>
                  </form>
        <?php endif;?>
      </div>
    </div>
  </div>
  <?php endforeach;?>
</div>
<?php require_once APPROOT."/views/templates/footer.php";?>