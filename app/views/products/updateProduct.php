<?php require_once APPROOT."/views/templates/header.php";?>
<form action="<?php echo URLROOT . "/products/updateProduct/" . htmlspecialchars($data['productid']) ?>" method="post" enctype="multipart/form-data">
  <div class="mb-3">
        <label  class="form-label">Enter the product name</label>
        <input type="text" name="productname" class="form-control" value="<?php echo htmlspecialchars($data['productname'])?>">
        <?php if($data['productnameError']):?>
            <div class='alert alert-danger mt-2'><?php echo htmlspecialchars($data['productnameError'])?></div>
        <?php endif;?>

  </div>
  <div class="mb-3">
    <label  class="form-label">Enter the product price</label>
    <input type="text" name="productprice" class="form-control" value="<?php echo htmlspecialchars($data['productprice'])?>">
    <?php if($data['productpriceError']):?>
            <div class='alert alert-danger mt-2'><?php echo htmlspecialchars($data['productpriceError'])?></div>
    <?php endif;?>
  </div>
  <div class="form-group">
    <label>Product description</label>
    <textarea class="form-control" rows="5" name='productdesc'> <?php echo htmlspecialchars($data['productdesc'])?> </textarea>
    <?php if($data['productdescError']):?>
            <div class='alert alert-danger mt-2'><?php echo htmlspecialchars($data['productdescError'])?></div>
    <?php endif;?>
  </div>
<div class="mb-3">
    <label  class="form-label">Insert an image</label>
    <input type="file" name="productimage" class="form-control">
    <?php if($data['productimageError']):?>
            <div class='alert alert-danger mt-2'><?php echo htmlspecialchars($data['productimageError'])?></div>
    <?php endif;?>
    
</div>
<button type='submit' name='btn' class='btn btn-info my-1'>submit</button>

</form>
<?php require_once APPROOT."/views/templates/footer.php";?>