<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- Latest compiled and minified CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">

</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-2">
  <div class="container">
    <a class="navbar-brand" href="<?php echo URLROOT?>/Products/index">E-shop</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <?php if(isset($_SESSION['user_id'])):?>
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="<?php echo URLROOT?>/users/logout">logout</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="<?php echo URLROOT?>/products/index">products</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="<?php echo URLROOT?>/orders/myorder">my orders</a>
          </li>
        <?php else:?>
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="<?php echo URLROOT?>/users/login">login</a>
          </li>
          
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="<?php echo URLROOT?>/users/signup">signup</a>
          </li>
        <?php endif;?>
      </ul>
    </div>
  </div>
</nav>

<div class="container">


