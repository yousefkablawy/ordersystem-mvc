<?php require_once APPROOT."/views/templates/header.php";?>
<form action="<?php echo URLROOT ?>/users/signup" method="POST">
<div class="mb-3">
    <label  class="form-label">Full Name:</label>
    <input type="text" name="fullname" class="form-control" >
    <?php if($data['fullnameError']):?>
            <div class='alert alert-danger mt-2'><?php echo htmlspecialchars($data['fullnameError'])?></div>
    <?php endif;?>
</div>
<div class="mb-3">
    <label  class="form-label">Email address</label>
    <input type="text" name="email" class="form-control">
    <?php if($data['emailError']):?>
            <div class='alert alert-danger mt-2'><?php echo htmlspecialchars($data['emailError'])?></div>
    <?php endif;?>
    
<div class="mb-3">
    <label  class="form-label">UserName:</label>
    <input type="text" name="username" class="form-control" >
    <?php if($data['usernameError']):?>
            <div class='alert alert-danger mt-2'><?php echo htmlspecialchars($data['usernameError'])?></div>
    <?php endif;?>

</div>
<div class="mb-3">
    <label  class="form-label">Password</label>
    <input type="password" name="password" class="form-control"  >
    <?php if($data['passwordError']):?>
            <div class='alert alert-danger mt-2'><?php echo htmlspecialchars($data['passwordError'])?></div>
    <?php endif;?>
</div>
<div class="mb-3">
    <label  class="form-label">Confirm Password</label>
    <input type="password" name="confirmPassword" class="form-control" >
    <?php if($data['confirmPasswordError']):?>
            <div class='alert alert-danger mt-2'><?php echo htmlspecialchars($data['confirmPasswordError'])?></div>
    <?php endif;?>
</div>
<button type="submit" class="btn btn-primary" name='btn'>Submit</button>
<div> <a class='text-primary' href="<?php echo URLROOT?>/users/login">Already have an account?</a> </div>
</form>
<?php require_once APPROOT."/views/templates/footer.php";?>