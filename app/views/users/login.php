<?php require_once APPROOT."/views/templates/header.php";?>
    <form action="<?php echo URLROOT?>/users/login" method="post">
    <div class="mb-3">
        <label  class="form-label">Email address OR email</label>
        <input type="text" name="username" class="form-control" placeholder='Username\email : ....'>
        <?php if($data['usernameError']):?>
            <div class='alert alert-danger mt-2'><?php echo htmlspecialchars($data['usernameError'])?></div>
        <?php endif;?>
    </div>
    <div class="mb-3">
        <label  class="form-label">Password</label>
        <input type="password" name="password" class="form-control" >
        <?php if($data['passwordError']):?>
            <div class='alert alert-danger mt-2'><?php echo htmlspecialchars( $data['passwordError'])?></div>
        <?php endif;?>
    </div>
    <button type="btn" name="submit" class="btn btn-primary">Submit</button>
    <div> <a class='text-primary' href="<?php echo URLROOT?>/users/signup">Rigster Now</a> </div>
    </form>
<?php require_once APPROOT."/views/templates/footer.php"?>;
