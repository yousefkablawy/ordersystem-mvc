<?php require_once APPROOT."/views/templates/header.php";?>
<table class="table table-hover">
        <thead>
        <tr>
            <th>Product name</th>
            <th>product price</th>
            <th>product desc</th>
            <th>product image</th>
            <th>product quantity</th>
            <th>Order Date</th>
        </tr>
        </thead>
        <tbody>
            <?php foreach($data['orders'] as $order):?>
                <tr>
                    <td><?php echo htmlspecialchars($order->productname)?></td>
                    <td><?php echo htmlspecialchars($order->productprice)?></td>
                    <td><?php echo htmlspecialchars($order->productdesc)?></td>
                    <td><img src="<?php echo "../public/img/".htmlspecialchars($order->filename)?>" class='w-25'></td>
                    <td><?php echo htmlspecialchars($order->productquantity)?></td>
                    <td><?php echo htmlspecialchars($order->orderdate)?></td>
                </tr>
            <?php endforeach;?>
        </tbody>
    </table>
<?php require_once APPROOT."/views/templates/footer.php";?>